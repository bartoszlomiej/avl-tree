#include <iostream>
#include <cmath>
#include <exception>

using namespace std;

class WrongKeyGiven : public exception{
  virtual const char* what() const throw(){
    return "Wrong key was given!";
  }
} wrongKey;

template<typename Key, typename Info>
class AVLTree{
  struct node;
public:
  AVLTree() : root(nullptr) {};
  ~AVLTree() { clear(); };
  AVLTree(const AVLTree<Key, Info> &x) {copyTree(x);};
  AVLTree<Key, Info> operator=(const AVLTree<Key, Info> &x);
  bool insert(const Key &k, const Info &inf);
  void printPre();
  void printIn();
  void printPost();
  void deleteNode(const Key &k);
  void clear();
  bool find(const Key &k) { if(search_node(k, root) == NULL) return false; return true; }
  const Info& getInfo(const Key &k) { if(find(k)) return search_node(k, root)->info; throw wrongKey;}
  void print();
  const Key& min() { return minimalVal(root); };
  const Key& max() { return maximalVal(root); };
private:
  node* root;
  int height(node* element); 
  auto* search_node(const Key &k, node* element);
  bool insert_element(node* element, const Key &k, const Info &inf);
  auto* rotateLeft(node* element);
  auto* rotateRight(node* element);
  void balanceTree(node** element, const Key &k);
  auto* deleteTheNode(node* element, node* parent, const Key &k);
  auto* predecessor(node* element, node* parent);
  auto* successor(node* element, node* parent);
  void deleteTree(node* element);
  void copyTree(const AVLTree<Key, Info> &x);
  auto* copyElements(node* sRoot);
  void printVertically(node* element, int n);
  void printPreorder(node* element);
  void printInorder(node* element);
  void printPostorder(node* element);
  Key& minimalVal(node* element);
  Key& maximalVal(node* element);
};



template<typename Key, typename Info>
bool AVLTree<Key, Info>::insert(const Key &k, const Info &inf){
  if(root == nullptr){
    root = new node(k, inf);
    return true;
  }
  insert_element(root, k, inf);
  balanceTree(&root, k);
  return true;
}


template<typename Key, typename Info>
bool AVLTree<Key, Info>::insert_element(node* element, const Key &k, const Info &inf){
  if(element->key < k){
    if(element->right == nullptr){
      element->right = new node(k, inf);
      return true;
    }
    return insert_element(element->right, k, inf);
  }
  else if(element->key > k){
    if(element->left == nullptr){
      element->left = new node(k, inf);
      return true;
    }
    return insert_element(element->left, k, inf);
  }
  return false;
}

template<typename Key, typename Info>
void AVLTree<Key, Info>::balanceTree(node** element, const Key &k){
  if((*element)->key < k){
    balanceTree(&(*element)->right, k);
  }
  else if((*element)->key > k){
    balanceTree(&(*element)->left, k);
  }
  
  (*element)->bfactor = height((*element)->right) - height((*element)->left);

  if(abs((*element)->bfactor) > 1){

    if((*element)->bfactor > 1){
      if(k > ((*element)->right)->key){ //right-right
	(*element) = rotateLeft(*element);
      }
      else{
	(*element)->right = rotateRight((*element)->right); //right-left
	(*element) = rotateLeft(*element);
	return;
      }
    }
    else{
      if(k < ((*element)->left)->key){ //left-left
	(*element) = rotateRight(*element);
      }
      else{
	(*element)->left = rotateLeft((*element)->left); //left-right
	(*element) = rotateRight(*element);
	return;
      }
    }
    
  }
}

template<typename Key, typename Info>
auto* AVLTree<Key, Info>::rotateLeft(node* element){
  if(element == nullptr)
    return element;

  node* newNode = element->right;
  element->right = newNode->left;
  newNode->left = element;
  if(element == root)
    root = newNode;

  element = newNode;
  return newNode;
}

template<typename Key, typename Info>
auto* AVLTree<Key, Info>::rotateRight(node* element){
  if(element == nullptr)
    return element;
  
  node* newNode = element->left;
  node* buffer = newNode->right;
  element->left = newNode->right;
  newNode->right = element;
  if(element == root)
    root = newNode;
  element = newNode;
  return newNode;
}


template<typename Key, typename Info>
auto* AVLTree<Key, Info>::search_node(const Key& k, node* element){
  if(element == NULL)
    return element; 
  if(element->key == k)
    return element;
  
  if(element->key < k)
    return search_node(k, element-> right);
  else  
    return search_node(k, element->left);
}

template<typename Key, typename Info>
void AVLTree<Key, Info>::deleteNode(const Key &k){
  node* newNode = deleteTheNode(root, root, k);
  if(newNode != nullptr){
    balanceTree(&root, newNode->key);
  }
}

template<typename Key, typename Info>
auto* AVLTree<Key, Info>::deleteTheNode(node* element, node* parent, const Key &k){
  /**It should return the pointer to the parent node of removed element*/

  if(element == nullptr)
    return element;
  if(k > element->key)
    return deleteTheNode(element->right, element, k);
  else if(k < element->key)
    return deleteTheNode(element->left, element, k);

  if((element->left == nullptr) || (element->right == nullptr)){
    node* buffer;
    if(element->left == nullptr){
      buffer = element;
      element = element->right;
    }
    else{
      buffer = element;
      element = element->left;
    }
    
    if(element != nullptr){
      if(k > parent->key)
	parent->right = element;
      else
	parent->left = element;
      delete buffer;
    }
    else{
      if(k > parent->key)
	parent->right = nullptr;
      else
	parent->left = nullptr;
      delete element;
    }
  }
  else{
    node* parentBuffer;
    node* childBuffer;
    if(k > parent->key){
      parentBuffer = successor(element->right, parent);
      childBuffer = parentBuffer->left;
      parentBuffer->left = childBuffer->right;
      childBuffer->right = element->right;
      childBuffer->left = element->left;
      parent->right = childBuffer;
      element->right = nullptr;
      element->left = nullptr;
      delete element;
    }
    else{
      parentBuffer = predecessor(element->left, parent);
      childBuffer = parentBuffer->right;
      parentBuffer->right = childBuffer->left;
      childBuffer->left = element->left;
      childBuffer->right = element->right;
      parent->left = childBuffer;
      if(element == root){
	root = childBuffer;
	parent = nullptr;
      }
      element->right = nullptr;
      element->left = nullptr;
      delete element;
    }
  }
  return parent;
}

template<typename Key, typename Info>
auto* AVLTree<Key, Info>::predecessor(node* element, node* parent){
  if(element->right == nullptr)
    return parent;
  return predecessor(element->right, element);
}

template<typename Key, typename Info>
auto* AVLTree<Key, Info>::successor(node* element, node* parent){
  if(element->left == nullptr)
    return parent;
  return successor(element->left, element);
}

template<typename Key, typename Info>
void AVLTree<Key, Info>::clear(){
  deleteTree(root);
  root = nullptr;
}

template<typename Key, typename Info>
void AVLTree<Key, Info>::deleteTree(node* element){
  if(element == nullptr)
    return;
  deleteTree(element->right);
  deleteTree(element->left);
  element->right = nullptr;
  element->left = nullptr;
  delete element;
}

template<typename Key, typename Info>
int AVLTree<Key, Info>::height(node* element){
  if(element == nullptr)
    return 0;

  int a = height(element->right);
  int b = height(element->left);

  if(a > b)
    return a + 1;
  return b + 1;
}

template<typename Key, typename Info>
void AVLTree<Key, Info>::copyTree(const AVLTree<Key, Info> &x){
  if(x.root == nullptr){
    root = nullptr;
    return;
  }
  node* secondRoot;
  secondRoot = x.root;
  root = copyElements(secondRoot);
  delete secondRoot;
}

template<typename Key, typename Info>
auto* AVLTree<Key, Info>::copyElements(node* sRoot){
  if(sRoot == nullptr){
    node* newNode = nullptr;
    return newNode;
  }
  else{
    node* newNode = new node(sRoot->key, sRoot->info);
    newNode->right = copyElements(sRoot->right);
    newNode->left = copyElements(sRoot->left);
    return newNode;
  }
}

template<typename Key, typename Info>
AVLTree<Key, Info> AVLTree<Key, Info>::operator=(const AVLTree<Key, Info> &x){
  if(this != &x){
    clear();
    copyTree(x);
  }
    
  return *this;
}

template<typename Key, typename Info>
void AVLTree<Key, Info>::print(){
  printVertically(root, 0);
}

template<typename Key, typename Info>
void AVLTree<Key, Info>::printVertically(node* element, int n){
  if(element != nullptr){
    if(element->right){
      printVertically(element->right, n + 5);
    }
    for( int i = 0; i < n; i++)
      cout << " ";
    if (element->right){
      cout << "  /" << endl;
      for(int i = 0; i < n; i++)
	cout << " ";
    }
    cout << element->key << endl;
    if(element->left){
      for(int i = 0; i < n; i++){
	cout << " ";
      }
      cout <<"  \\" << endl;
      printVertically(element->left, n + 5);
    }
  }

}

template<typename Key, typename Info>
void AVLTree<Key, Info>::printPre(){
  if(!root)
    cout << "empty tree" << endl;
  printPreorder(root);
}

template<typename Key, typename Info>
void AVLTree<Key, Info>::printIn(){
  if(!root)
    cout << "empty tree" << endl;
  printInorder(root);
}

template<typename Key, typename Info>
void AVLTree<Key, Info>::printPost(){
  if(!root)
    cout << "empty tree" << endl;
  printPostorder(root);
}

template<typename Key, typename Info>
void AVLTree<Key, Info>::printPreorder(node* element){ //change print!!!
  if(element == nullptr)
    return;
  cout << "Key: " << element->key << endl;
  cout << "Info: " << element->info << endl << endl;
  printPreorder(element->right);
  printPreorder(element->left);
}

template<typename Key, typename Info>
void AVLTree<Key, Info>::printInorder(node* element){ //change print!!!
  if(element == nullptr)
    return;
  printInorder(element->right);
  cout << "Key: " << element->key << endl;
  cout << "Info: " << element->info << endl << endl;
  printInorder(element->left);
}

template<typename Key, typename Info>
void AVLTree<Key, Info>::printPostorder(node* element){ //change print!!!
  if(element == nullptr)
    return;
  printPostorder(element->right);
  printPostorder(element->left);
  cout << "Key: " << element->key << endl;
  cout << "Info: " << element->info << endl << endl;
}

template<typename Key, typename Info>
Key& AVLTree<Key, Info>::minimalVal(node* element){
  if(!element->left)
    return element->key;
  return minimalVal(element->left);
}

template<typename Key, typename Info>
Key& AVLTree<Key, Info>::maximalVal(node* element){
  if(!element->right)
    return element->key;
  return maximalVal(element->right);
}

template<typename Key, typename Info>
struct AVLTree<Key, Info>::node{
  node(const Key &k, const Info &inf) : key(k), info(inf),
  					left(nullptr),
  					right(nullptr), bfactor(0) {}
  node() : key(0), info(0), left(nullptr), right(nullptr), bfactor(0) {}
  Key key;
  Info info;
  node* left;
  node* right;
  int bfactor;
};
