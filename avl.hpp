#include <iostream>
#include <cmath>

using namespace std;

template<typename Key, typename Info>
class AVLTree{
  struct node;
public:
  AVLTree() : root(nullptr) {};
  bool insert(Key &k, Info &inf);
  void temp_print();
  int h_print(Key &k) {return height(search_node(k, root), 0);} //to remove
  //delete
  //min
  //max
  //next-larger
  //next-smaller
  //  bool find(Key &k) { if(search_node(k, root) == NULL) return false; return true; }
private:
  node* root;
  int height(node* element, int n); //---------needed???????????
  auto* search_node(Key &k, node* element);
  void t_print(node* element);
  bool insert_element(node* element, Key &k, Info &inf);
  void rotateLeft(node* element);
  void rotateRight(node* element);
  void update_bfactor(node* element);
  void balanceTree(node** element, Key &k);
  void pom(node* element, Key &k);
};



template<typename Key, typename Info>
bool AVLTree<Key, Info>::insert(Key &k, Info &inf){
  if(root == nullptr){
    root = new node(k, inf);
    return true;
  }
  insert_element(root, k, inf);
  //  pom(root, k);
  balanceTree(&root, k);
  return true;
}


template<typename Key, typename Info>
bool AVLTree<Key, Info>::insert_element(node* element, Key &k, Info &inf){
  if(element->key < k){
    if(element->right == nullptr){
      element->right = new node(k, inf);
      return true;
    }
    return insert_element(element->right, k, inf);
  }
  else if(element->key > k){
    if(element->left == nullptr){
      element->left = new node(k, inf);
      return true;
    }
    return insert_element(element->left, k, inf);
  }
  return false;
}

template<typename Key, typename Info>
void AVLTree<Key, Info>::balanceTree(node** element, Key &k){
  if((*element)->key < k){
    balanceTree(&(*element)->right, k);
  }
  else if((*element)->key > k){
    balanceTree(&(*element)->left, k);
  }
  //  if(!element) return false;
  //  update_bfactor(root);
  /*
  if(k > element->key)
    return balanceTree(element->right, k);
  else if(k < element->key)
    return balanceTree(element->left, k);
  */
  (*element)->bfactor = height((*element)->right, 0) - height((*element)->left, 0);
  cout << "Element: " << (*element)->key << " Height: " << (*element)->bfactor << endl;
  //  cout << height(element->left) << endl;
  //  cout << element->bfactor << endl;
  //  temp_print();
  if(abs((*element)->bfactor) > 1){

    //check if element is a root (we cannot check the parent of root)??
    //    if(parent == nullptr){ //correct?
    //      cerr << "check this case" << endl;
    //      return;

    if((*element)->bfactor > 1){
      if(k > ((*element)->right)->key){ //right-right
	//element = rotateLeft(element);
	rotateLeft(*element);
      }
      else{
	cout << "right-left" << endl;
	//	element->right = rotateRight(element->right); //right-left
	//	element = rotateLeft(element);
	cout <<  "Przed: " << (*element)->key << endl;
	rotateRight((*element)->right); //right-left
	cout <<  "Mid: " << (*element)->key << endl;
	rotateLeft(*element);
	cout <<  "After: " << (*element)->key << endl;
	return;
      }
    }
    else{
      if(k < ((*element)->left)->key){ //left-left
	//	element = rotateRight(element);
	rotateRight(*element);
      }
      else{
	//	element->left = rotateLeft(element->left); //left-right
	//	element = rotateRight(element);
	rotateLeft((*element)->left); //left-right
	rotateRight(*element);
	return;
      }
    }
    
  }
}

template<typename Key, typename Info>
void AVLTree<Key, Info>::rotateLeft(node* element){
  //  if(element == nullptr)
  //    return element;
  //  if(!element->right){
    // element = element->right;
  //  return element;
  //  }
  //  else{
    cout << "Przed ele: " << element->key << endl;
    node* newNode = element->right;
    //    if(newNode != nullptr)
    element->right = newNode->left;
    newNode->left = element;
    if(element == root)
      root = newNode;
    element = newNode;
    cout << "ele: " << element->key << endl;
    //    cout << element->right->key << endl;
    //    cout << element->left->key << endl;
    //    return element;
    //  }
}

template<typename Key, typename Info>
void AVLTree<Key, Info>::rotateRight(node* element){
  //  if(element == nullptr)
    //    return element;
  //   if(!element->left){
  //     cout << element->key << endl;
     //     element = element->right;
  //     return element;
  //   }
  //  else{
    cout << "Przed ele right: " << element->key << endl;
    node* newNode = element->left;
    //    if(newNode != nullptr)
      element->left = newNode->right;
      newNode->right = element;
      /*    }
    else{
      newNode = element->right;
      element->right = newNode->left;
      (element->right)->right = newNode;
    }*/
      if(element == root)
	root = newNode;
    element = newNode;
    cout << "ele: " << element->key << endl;
    //    cout << element->right->key << endl;
    //    cout << element->left->key << endl;
    //    return element;
    //  }
}

template<typename Key, typename Info>
void AVLTree<Key, Info>::update_bfactor(node* element){
  if(element == nullptr)
    return;
  element->bfactor = height(element->right, 0) - height(element->left, 0);
  update_all_bfactors(element->right); //could be done without parent
  update_all_bfactors(element->left);
}

template<typename Key, typename Info>
auto* AVLTree<Key, Info>::search_node(Key& k, node* element){
  if(element == NULL)
    return element; //returns NULL
  if(element->key == k)
    return element;
  
  if(element->key > k)  //on the right there are greater elements
    return search_node(k, element-> right);
  
  return search_node(k, element->left); //and on the left smaller elements
}

template<typename Key, typename Info>
int AVLTree<Key, Info>::height(node* element, int n){
  if(element == nullptr)
    return n;
  n++;
  int a = height(element->right, n);
  int b = height(element->left, n);
  if(a > b)
    return a;
  return b;
}

template<typename Key, typename Info>
void AVLTree<Key, Info>::temp_print(){
  t_print(root);
}

template<typename Key, typename Info>
void AVLTree<Key, Info>::t_print(node* element){ //change print!!!
  if(element != NULL){
    cout << "Key: " << element->key << endl;
    cout << "Info: " << element->info << endl << endl;
    t_print(element->right);
    t_print(element->left);
  }
  else{
    return;
  }
}

template<typename Key, typename Info>
struct AVLTree<Key, Info>::node{
  node(Key &k, Info &inf) : key(k), info(inf),
  					left(nullptr),
  					right(nullptr), bfactor(0) {}
  Key key;
  Info info;
  node* left;
  node* right;
  int bfactor;
};
