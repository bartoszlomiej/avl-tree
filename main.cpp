#include <iostream>
#include "avl.hpp"

using namespace std;

int main(){
  int a = 40, b =99;
  AVLTree<int, int> tree;
  cout << tree.insert(a, b) << endl;
  a = 50;
  b = 123;
  cout << tree.insert(a, b) << endl;
  a = 60;
  b = 1;
  cout << tree.insert(a, b) << endl;
  /*  
  a = 60;
  b = 2;
  cout << tree.insert(a, b) << endl;
  a = 50;
  b = 2;
  cout << tree.insert(a, b) << endl;

  a = 80;
  b = 2;
  cout << tree.insert(a, b) << endl;
  a = 15;
  b = 2;
  cout << tree.insert(a, b) << endl;
  a = 28;
  b = 2;
  cout << tree.insert(a, b) << endl;
  a = 25;
  b = 2;
  cout << tree.insert(a, b) << endl;
  */
  a = 40;
  cout << "height: " << tree.h_print(a) << endl << endl;
  tree.temp_print();
  //  cout << tree.height(a) << endl;
}
