#include <iostream>
#include "v3.hpp"
#include <exception>

using namespace std;

int main(){
  int a = 40, b =99;
  AVLTree<int, int> tree;
  cout << tree.insert(a, b) << endl;
  a = 30;
  b = 123;
  cout << tree.insert(a, b) << endl;
  a = 20;
  b = 1;
  cout << tree.insert(a, b) << endl;
  
  a = 60;
  b = 2;
  cout << tree.insert(a, b) << endl;
  a = 70;
  b = 2;
  cout << tree.insert(a, b) << endl;
  
  //  tree.dbg();
  
  a = 80;
  b = 2;
  cout << tree.insert(a, b) << endl;

  a = 15;
  b = 2;
  cout << tree.insert(a, b) << endl;
  a = 28;
  b = 2;
  cout << tree.insert(a, b) << endl;
    
  a = 25;
  b = 2;
  cout << tree.insert(a, b) << endl;
  /*
  a = 40;
  cout << "height: " << tree.h_print(a) << endl << endl;
  */
  tree.printPre();
  cout << "Testing of the delete." << endl;
  a = 30;
  tree.deleteNode(a);
  tree.printPre();

  cout << "Tree2: " << endl;
  AVLTree<int, int> tree2(tree);
  tree2.printPre();
  cout << "Is there 25? " << tree2.find(25) << endl;
  tree2.deleteNode(60);
  tree2.printPre();
  cout << "Get element 28 " << tree2.getInfo(28) << endl;
  try{
    cout << "Get element 31 " << tree2.getInfo(31) << endl;
  }
  catch(exception &e){
    cerr << e.what() << endl;
  }
  tree2.print();
  cout << "Min: " << tree2.min() << endl;
  cout << "Max: " << tree2.max() << endl;
  tree.clear();
  //  cout << tree.height(a) << endl;
}

